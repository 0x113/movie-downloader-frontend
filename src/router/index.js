import Vue from 'vue'
import Router from 'vue-router'
import CdaDownload from '@/components/CdaDownload'
import FiliDownload from '@/components/FiliDownload'
import Homepage from '@/components/Homepage'
import OtherProjects from '@/components/OtherProjects'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: "/",
      name: 'Homepage',
      component: Homepage
    },
    {
      path: '/cda',
      name: 'CdaDownload',
      component: CdaDownload
    },
    {
      path: '/fili',
      name: 'FiliDownload',
      component: FiliDownload
    },
    {
      path: "/projects",
      name: "OtherProjects",
      component: OtherProjects
    }
  ]
})
